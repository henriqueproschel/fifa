from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, get_object_or_404
from django.db.models import Q, F, fields
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView 

from .models import Competition
from .forms import  CompetitionForm

# Create your views here.

def search_competitions(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        competition_list = Competition.objects.filter(
                                Q(name__icontains=search_term) | 
                                Q(region__icontains=search_term) 
                                )
        context = {"competition_list": competition_list}
    return render(request, 'competitions/search.html', context)

class CompetitionDetailView(DetailView):
    model = Competition
    template_name = 'competitions/detail.html'
    queryset = model.objects.all()
    
    def get_object(self):
        competition_id = self.kwargs.get("pk")
        return get_object_or_404(Competition, pk=competition_id)
    
class CompetitionListView(ListView):
    model = Competition
    template_name = 'competitions/index.html'
    queryset = model.objects.all()

class CompetitionCreateView(CreateView):
    model = Competition
    template_name = 'competitions/create.html'
    form_class = CompetitionForm
    queryset = model.objects.all()
    
    def form_valid(self, form):
        return super().form_valid(form)
    
class CompetitionUpdateView(UpdateView):
    model = Competition
    template_name = 'competitions/update.html'
    form_class = CompetitionForm
    queryset = model.objects.all()

    def form_valid(self, form):
        return super().form_valid(form)
    def get_object(self):
        competition_id = self.kwargs.get("pk")
        return get_object_or_404(Competition, pk=competition_id)

class CompetitionDeleteView(DeleteView):
    model = Competition
    template_name = 'competitions/delete.html'
    queryset = model.objects.all()

    def get_object(self):
        competition_id = self.kwargs.get("pk")
        return get_object_or_404(Competition, pk=competition_id)
    def get_success_url(self):
        return reverse('competitions:index')