from django.urls import path

from . import views

app_name = 'competitions'
urlpatterns = [
    path('', views.CompetitionListView.as_view(), name='index'),
    path('<int:pk>/', views.CompetitionDetailView.as_view(), name='detail'),
    path('search/', views.search_competitions, name='search'),
    path('create/', views.CompetitionCreateView.as_view(), name='create'),
    path('<int:pk>/update/', views.CompetitionUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', views.CompetitionDeleteView.as_view(), name='delete'),

]