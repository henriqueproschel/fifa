from django.db import models
from django.utils import timezone
from django.conf import settings
from django.urls import reverse

from teams.models import Team

# Create your models here.
class Competition(models.Model):
    "Categorizacao das paginas de equipes"
    post_date    = models.DateTimeField(default = timezone.now)
    name         = models.CharField(max_length=100)
    first_ed     = models.IntegerField()
    logo_url     = models.URLField(max_length=500, null=True)
    region       = models.CharField(max_length=100)
    organization = models.CharField(max_length=100)
    about        = models.TextField(default="")
    teams        = models.ManyToManyField(Team, related_name="competitions", blank=True)

    def get_absolute_url(self):
        return reverse("competitions:detail", kwargs={"pk":self.pk})

    def __str__(self):
        return f'{self.name}'
