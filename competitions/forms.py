from django.forms import ModelForm
from .models import Competition
from teams.models import Team

class CompetitionForm(ModelForm):
    class Meta:
        model = Competition
        fields = [
            'name',
            'first_ed',
            'logo_url',
            'region',
            'organization',
            'teams',
            'about'
        ]
        labels = {
            'name': 'Nome da competição',
            'first_ed': 'Primeira edição',
            'logo_url': 'URL do logo',
            'region': 'Região de disputa',
            'organization': 'Organização',
            'teams': 'Equipes participantes',
            'about':'Sobre a competição'
        }
    
    def __init__(self, *args, **kwargs):
        super(CompetitionForm, self).__init__(*args, **kwargs)   
        self.fields['teams'].queryset = Team.objects.order_by('name')