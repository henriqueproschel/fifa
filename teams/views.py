from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render, get_object_or_404
from django.db.models import Q, fields
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView 

from .models import Team, Comment
from .forms import TeamForm, CommentForm

# Create your views here.

def search_teams(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        team_list = Team.objects.filter(Q(name__icontains=search_term) | Q(country__icontains=search_term) )
        context = {"team_list": team_list}
    return render(request, 'teams/search.html', context)

class TeamDetailView(DetailView):
    model = Team
    template_name = 'teams/detail.html'
    queryset = model.objects.all()
    
    def get_object(self):
        team_id = self.kwargs.get("pk")
        return get_object_or_404(Team, pk=team_id)

class TeamListView(ListView):
    model = Team
    template_name = 'teams/index.html'
    queryset = model.objects.all()

class TeamCreateView(CreateView):
    model = Team
    template_name = 'teams/create.html'
    form_class = TeamForm
    queryset = model.objects.all()

    def form_valid(self, form):
        return super().form_valid(form)

class TeamUpdateView(UpdateView):
    model = Team
    template_name = 'teams/update.html'
    form_class = TeamForm
    queryset = model.objects.all()

    def form_valid(self, form):
        return super().form_valid(form)
    def get_object(self):
        team_id = self.kwargs.get("pk")
        return get_object_or_404(Team, pk=team_id)

class TeamDeleteView(DeleteView):
    model = Team
    template_name = 'teams/delete.html'
    queryset = model.objects.all()

    def get_object(self):
        team_id = self.kwargs.get("pk")
        return get_object_or_404(Team, pk=team_id)
    def get_success_url(self):
        return reverse('teams:index')

def post_comment(request, pk):
    team = get_object_or_404(Team, pk=pk)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                              text=comment_text,
                              team=team)
            comment.save()
            return HttpResponseRedirect(
                reverse('teams:detail', args=(pk, )))
    else:
        form = CommentForm()
    context = {'form': form, 'team': team}
    return render(request, 'teams/comment.html', context)