from django.forms import ModelForm, ModelMultipleChoiceField
from .models import Team, Comment
from competitions.models import Competition

class TeamForm(ModelForm):
    class Meta:
        model = Team
        fields = [
            'name',
            'full_name',
            'logo_url',
            'foundation',
            'base_city',
            'country',
            'about',
        ]
        labels = {
            'name' : 'Nome da equipe',
            'full_name': 'Nome completo da equipe',
            'logo_url' : 'URL do escudo',
            'foundation' : 'Ano de fundação',
            'base_city' : 'Cidade sede',
            'country' : 'País',
            'about' : 'Sobre a equipe',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Autor',
            'text': 'Comentário',
        }