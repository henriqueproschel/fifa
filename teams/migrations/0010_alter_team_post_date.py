# Generated by Django 3.2.7 on 2021-11-26 00:11

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0009_alter_team_post_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='post_date',
            field=models.DateTimeField(default=datetime.datetime(2021, 11, 26, 0, 11, 27, tzinfo=utc)),
        ),
    ]
