from django.urls import path

from . import views

app_name = 'teams'
urlpatterns = [
    path('', views.TeamListView.as_view(), name='index'),
    path('<int:pk>/', views.TeamDetailView.as_view(), name='detail'), 
    path('search/', views.search_teams, name='search'),
    path('create/', views.TeamCreateView.as_view(), name='create'),
    path('<int:pk>/update/', views.TeamUpdateView.as_view(), name='update'),
    path('<int:pk>/delete/', views.TeamDeleteView.as_view(), name='delete'),
    path('<int:pk>/comment/', views.post_comment, name='comment'),
]