from django.db import models
from django.utils import timezone
from django.conf import settings
from django.urls import reverse

# Create your models here.
class Team(models.Model):
    post_date    = models.DateTimeField(default = timezone.now)
    name         = models.CharField(max_length=100)
    full_name    = models.CharField(max_length=100)        
    logo_url     = models.URLField(max_length=500, null=True)
    foundation   = models.IntegerField()
    base_city    = models.CharField(max_length=100)
    country      = models.CharField(max_length=100)
    about        = models.TextField(default="")

    def get_absolute_url(self):
        return reverse("teams:detail", kwargs={"pk": self.pk})
    
    def __str__(self):
        return f'{self.name}'    

class Comment(models.Model):
    post_date  = models.DateTimeField(default = timezone.now)
    author     = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    text       = models.TextField(max_length=255)
    likes      = models.IntegerField(default=0)
    team       = models.ForeignKey(Team, related_name="comments", on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse("teams:detail", kwargs={"pk": self.team.pk})

    def __str__(self):
        return f'{self.author.username} - "{self.text}"'
