Esse diretório foi desenvolvido para a inserção de dados na aplicação FIFA - Fussball-Info für Alle!
O site desenvolvido com Django se encontra no endereço: http://fussballinfofuralle.herokuapp.com/
Os dados aqui encontrados foram majoritariamente retirados e/ou adaptados de Wikipedia.

Na aplicação, existem duas formas de dados: equipes e competições. Ambas possuem implementação das funções CRUD no site.

Para inserir um dos dados aqui apresentados no site:
- Se dirija a página de criação do conteúdo que deseja inserir:
    /teams/create - para inserir uma nova equipe ou;
    /competitions/create - para inserir uma nova competição.
- Os dados a serem inseridos nos campos (com exceção do campo "Sobre") encontram-se nos arquivos TeamData e
    CompetitionData, disponíveis nos formatos CSV e JSON.
- Escolha um dos dados e preencha os campos
- O preenchimento do campo "Sobre" pode ser feito com códigos HTML, e cada dado possue um arquivo correspondente nas
    pasta TeamHTML ou CompetitionHTML
- Encontre o arquivo da equipe* ou competição que deseja inserir, copie o código e cole código HTML no campo "Sobre" e clique em CRIAR
